#!/usr/bin/perl
######################################################################
#
# Name - deploy.pl
# Author - Jim Henk
# Date - 10/9/2018
#
#      Script to be called from Jenkins "Validate and Deploy" job
# Behavior is dictated by parameter choices in Jenkins job including:
#    Deploy server, forward deploy or revert operation, combination level of service
#       (code checkout and compile, report and deploy, full report or only adds and changes)
#  on deploy action: (depends on selected options)
#   1) Creates empty intermediate (staging) directory tree per server
#   2) Removes fileset from tree that must not be copied or reported on
#   3) Produces report, including file of non-tracked files in server tree
#   4) Deploys to server from intermediate staging area, and configs if action selected
#  on revert action:
#   1) Verifies intermediate staging area
#   2) Produces report of revert actions
#   3) Reverses actions of deploy operation, and configs if action selected
#
######################################################################
use File::Basename;
use File::Path qw(make_path remove_tree);
use File::Copy;
use File::Spec;
use Digest::SHA1  qw(sha1 sha1_hex sha1_base64);
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);
use Cwd;
use Win32::NetResource qw(:DEFAULT GetSharedResources GetError);
use Switch;

use constant wbemFlagReturnImmediately => 0x10;
use constant wbemFlagForwardOnly => 0x20;
 
# temporary re-mapping of array indices until Git Shareview/Configs matches names in Environments Confluence page
my %ip_array = (
  'QA'        => "192.168.4.8",
  'QA1'       => "192.168.97.11",
  'QA2'       => "192.168.97.7",
  'STAGE'    => "192.168.14.12",
  'CS1'    => "192.168.42.7",
  'CS2'    => "192.168.96.5",
);

my %dest_array = (
  'QA'        => "web\\sports_1_7",
  'QA1'       => "WwwRoot\\adc\\sports.affinitydev.com\\site",
  'QA2'       => "WwwRoot\\adc\\sports.affinitydev.com\\site",
  'STAGE'    => "SPTSWEB",
  'CS1'    => "WwwRoot\\adc\\sports.affinitydev.com\\site",
  'CS2'    => "WwwRoot\\adc\\sports.affinitydev.com\\site",
);

my %acctname_array = (
  'QA'        => $ENV{"LDAP_username"},
  'QA1'       => "localadmin",
  'QA2'       => "ciadmin",
  'STAGE'    => $ENV{"LDAP_username"},
  'CS1'    => "dmzadmin",
  'CS2'    => "dmzadmin",
);

my %password_array = (
  'QA'        => $ENV{"LDAP_password"},
  'QA1'       => "achinFP8",
  'QA2'       => "Za4>AjHy4d",
  'STAGE'    => $ENV{"LDAP_password"},
  'CS1'    => "achinFP8",
  'CS2'    => "achinFP8",
);
 
my $server_dir = "";
my $remote = "";
my $ip = "";
my $username = "";
my $password = "";

# Where exactly are we deploying *to* ?  Is it a local dev folder, or a remote server?
my $server_environment = $ENV{"Target"};

switch($server_environment) {
  case "Local" {
    $server_dir = "C:\\Workspaces\\Local";
	$remote = "false";
  }
  case "LocalQA2" {
    $server_dir = "C:\\Workspaces\\QA2";
	$remote = "false";
  } else {
    $ip = $ip_array{$server_environment};
    $server_dir = "\\\\" . $ip . "\\" . $dest_array{$server_environment};
    $username = $acctname_array{$server_environment};
    $password = $password_array{$server_environment};
	$remote = "true";
  }
}

# local build workspace with path to Shareview area
my $src_dir = $ENV{"WORKSPACE"} . "\\ShareView\\web\\sports_1_7";

# Where are the source config files?
my $src_config_dir;
switch($server_environment) {
  case "Local" {
    $src_config_dir = "c:\\users\\public\\Documents\\local_configs\\Local";
  } case "LocalQA2" {
    $src_config_dir = "c:\\users\\public\\Documents\\local_configs\\LocalQA2";
  } else {
    $src_config_dir = $ENV{"WORKSPACE"} . "\\ShareView\\Configs\\" . $server_environment;
  }
}

my $intermediate_dir = "c:\\users\\public\\Documents\\" . $ENV{"Target"} . "_intermediate"; 
my $not_tracked_file = "c:\\users\\public\\Documents\\" . $ENV{"Target"} . "_not_tracked.txt";
my $regex_file = "c:\\users\\public\\Documents\\regexlist.txt";
my $operation = $ENV{"Operation"};
my $DeployOrRevert = $ENV{"DeployOrRevert"};
my $not_tracked_filehandle;
my $Include_Deletes_and_Not_Tracked = $ENV{'Include_Deletes_and_Not_Tracked'};
my $Deploy_web_configs = $ENV{'Deploy_web_configs'};

my @regex_array;
 
load_regex_array();

if (($operation eq "Load_and_Report_Only") || ($operation eq "Load_Report_and_Deploy")) {
  $do_report = "true";  # Main switch for report function
}
 
if ($operation eq "Load_Report_and_Deploy") {
  $do_deploy = "true";  # Main switch for deploy function
}
 
# Intermediate directory paths (only used when actual deploy operation is selected)
my $add_dir = $intermediate_dir . "\\add";
my $delete_dir = $intermediate_dir . "\\delete";
my $old_version_dir = $intermediate_dir . "\\old_version";
my $new_version_dir = $intermediate_dir . "\\new_version";
my $configs_dir = $intermediate_dir . "\\configs";
my $shareview_int_config_dir = $intermediate_dir . "\\configs\\Shareview";
my $foundation_int_config_dir = $intermediate_dir . "\\configs\\Foundation";
my $shareview_int_config_revert_dir = $intermediate_dir . "\\configs\\revert\\Shareview";
my $foundation_int_config_revert_dir = $intermediate_dir . "\\configs\\revert\\Foundation";
 
# Connect to remote host if we're doing more than just the 'load' operation
if (($do_report == "true" ) or ($do_deploy eq "true")) {
  if ($remote eq "true") {
    print "***************************************************************************\n";
    my %RemoteShare = (
      'RemoteName' => "\\\\" . $ip,
    );
    print "\nConnecting to remote: $server_dir... ";
    if (Win32::NetResource::AddConnection(\%RemoteShare, $password, $username, 1)) {
      print "OK\n";
      print "\nVerifying $server_dir...\n";
      if (! -d $server_dir) {
        print "Hmmm... Cannot find it.  Quitting...\n";
        exit(1);
      } else {
        print "Verification successful.\n";
	  }
    } else {
      my $err;
      GetError($err);
      print "FAIL...\n";
      print "$err\n";
      exit(1);
    }
  } else {
    print "Verifying $server_dir... ";
    if (-d $server_dir) {
	  print "OK\n";
	} else {
	  print "FAIL - quitting\n";
	  exit(1);
	}
  }
}
 
if ($do_deploy eq "true") {
  if ($DeployOrRevert eq "Deploy") {
    print "***************************************************************************\n";
    print "(Re)creating intermediate directories.\n";
    create_inter_dirs();
  } elsif ($DeployOrRevert eq "Revert") {
    print "***************************************************************************\n";
    print "Verifying intermediate dirs...\n";
    verify_inter_dirs();
  }
}
 
print "***************************************************************************\n";
 
if ($DeployOrRevert eq "Deploy") {
  print "Removing files (and directories) from local build workspace that *must not* be copied to destination.\n";
  delete_undesirable_folders();
}
 
if ($do_report eq "true") {
  print "***************************************************************************\n";
  if ($operation eq "Load_and_Report_Only") {
    print "    R E P O R T  O N L Y\n";
    print "    --------------------\n";
  }

  if ($DeployOrRevert eq "Deploy") {
    print_deploy_delta_list();   # Report on main site and (optionally) populate intermediate dirs
	if ($Deploy_web_configs eq "Deploy") {
	  print_deploy_configs_src();    # Report on config files and (optionally) populate intermediate config dir
	}
  } elsif ($DeployOrRevert eq "Revert") {
	print_revert_delta_list();   # Report on main site and (optionally) populate intermediate dirs
	if (-e $shareview_int_config_revert_dir . "\\web.config") {
	  print_revert_configs_src();    # Report on reverting config files and (optionally) revert server config dir
	}
  }
}
 
if ($do_deploy eq "true") {
  print "***************************************************************************\n";
  if ($DeployOrRevert eq "Deploy") {
    print "Performing Deploy actions.\n";
    deploy_from_int_to_dest($DeployOrRevert);
	if ($Deploy_web_configs eq "Deploy") {
	  deploy_web_configs_to_int_from_Shareview_Configs();
	}
  } elsif ($DeployOrRevert eq "Revert") {
    print "Performing Revert actions...\n";
    deploy_from_int_to_dest($DeployOrRevert);
	if (-e $shareview_int_config_revert_dir . "\\web.config") {
	  revert_web_configs_from_intermediate_configs_dir();
	}
    print "Deleting intermediate directories...\n";
    remove_tree($intermediate_dir);
  }
}

######################################################################
#
# Routine - build_database_dir
# input - none
# output - none
#
#  
#
######################################################################
sub build_database_dir {
  my $PR_number = shift;
  
  my $conflict_flag = "false";
  my $file_conflict = "false";
  my $conflict_count = 0;
  my $row;
  my $in_db_file = "false";

  print "Generating database directory structure...\n";
  my @results = `curl -L -D- -X GET -H "Authorization: Basic amFtZXMuaGVua0BhZmZpbml0eS1zcG9ydHMuY29tOkVhZ2xlYnlGRUQyMDE4IQ==" -H "Content-Type: application/json"  https://bitbucket.org/!api/2.0/repositories/tshqdevelopment/affinity/pullrequests/245/diff`;

  foreach $row (@results) {
    chomp $row;
    $row =~ s{/}{\\}g;

    if ($row =~ /^diff --git a\/([\w|\-|\.|\s|\/]*)\sb\//) {
      $filename = $1;
      # print "Debug - $filename\n";
      if ($filename =~ /^Database/) {
        $in_db_file = "true";
      } else {
        $in_db_file = "false";
      }

      if ($in_db_file eq "true") {
        print "\n$filename";
        $index++;
        $file_conflict = "false"
      }

  	} elsif ($row =~ /<<<<<<</) {
      if ($in_db_file eq "true") {
        if ($file_conflict ne "true") {
          print "   >>>>>> CONFLICT!";
          $file_conflict = "true";
          $conflict_flag = "true";
          $conflict_count++;
        }
      }
    }
  }

  print "\n\n";

  if ($conflict_flag eq "true") {
    print "******* There were $conflict_count unresolved conflicts!\n";
  }
}

######################################################################
#
# Routine - print_deploy_configs_src
# input - none
# output - none
#
#  
#
######################################################################
sub print_deploy_configs_src {
  print "***************************************************************************\n";
  print "will replace " . $server_dir . "\\web.config with " . $src_config_dir . "\\Shareview\\web.config\n";
  print "will replace " . $server_dir . "\\Foundation\\web.config with " . $src_config_dir . "\\Foundation\\web.config\n";
  print "***************************************************************************\n";
}

######################################################################
#
# Routine - print_revert_configs_src
# input - none
# output - none
#
#  
#
######################################################################
sub print_revert_configs_src {
  print "***************************************************************************\n";
  print "will restore " . $server_dir . "\\web.config from " . $shareview_int_config_revert_dir . "\\web.config\n";
  print "will restore " . $server_dir . "\\Foundation\\web.config from " . $foundation_int_config_revert_dir . "\\web.config\n";
  print "***************************************************************************\n";
}

######################################################################
#
# Routine - deploy_web_configs_to_int_from_Shareview_Configs
# input - none
# output - none
#
#  Copies web.config files from local build workspace to intermediate config dirs,
#  copies destination original web.config files to intermediate revert folders,
#  and finally copies web.config files from intermediate config dirs to destination server
#
######################################################################
sub deploy_web_configs_to_int_from_Shareview_Configs {
  print "***************************************************************************\n";
  print "Populating intermediate config file area...\n";
  print "Copying " . $src_config_dir . "\\Shareview\\web.config to " . $shareview_int_config_dir . "\\web.config\n";
  copy($src_config_dir . "\\Shareview\\web.config", $shareview_int_config_dir . "\\web.config") or die "Copy failed: $!";
  print "Copying " . $src_config_dir . "\\Foundation\\web.config to " . $foundation_int_config_dir . "\\web.config\n";
  copy($src_config_dir . "\\Foundation\\web.config", $foundation_int_config_dir . "\\web.config") or die "Copy failed: $!";

  print "\nPopulating intermediate config revert file area from destination config files...\n";
  print "Copying " . $server_dir . "\\web.config to " . $shareview_int_config_revert_dir . "\\web.config\n";
  copy($server_dir . "\\web.config", $shareview_int_config_revert_dir . "\\web.config") or die "Copy failed: $!";
  print "Copying " . $server_dir . "\\Foundation\\web.config to " . $foundation_int_config_revert_dir . "\\web.config\n";
  copy($server_dir . "\\Foundation\\web.config", $foundation_int_config_revert_dir . "\\web.config") or die "Copy failed: $!";

  print "\nCopying intermediate config files to destination server...\n";
  print "Copying " . $shareview_int_config_dir . "\\web.config to " . $server_dir . "\\web.config\n";
  copy($shareview_int_config_dir . "\\web.config", $server_dir . "\\web.config") or die "Copy failed: $!";
  print "Copying " . $foundation_int_config_dir . "\\web.config to " . $server_dir . "\\Foundation\\web.config\n";
  copy($foundation_int_config_dir . "\\web.config", $server_dir . "\\Foundation\\web.config") or die "Copy failed: $!";

  print "***************************************************************************\n";
}

######################################################################
#
# Routine - revert_web_configs_from_intermediate_configs_dir
# input - none
# output - none
#
#  Copies web.config files from intermediate revert dirs to destination server  
#
######################################################################
sub revert_web_configs_from_intermediate_configs_dir {
  print "***************************************************************************\n";
  print "\nReverting intermediate config revert file area to destination config files...\n";
  print "Copying " . $shareview_int_config_revert_dir . "\\web.config to " . $server_dir . "\\web.config\n";
  copy($shareview_int_config_revert_dir . "\\web.config", $server_dir . "\\web.config") or die "Copy failed: $!";
  print "Copying " . $foundation_int_config_revert_dir . "\\web.config to " . $server_dir . "\\Foundation\\web.config\n";
  copy($foundation_int_config_revert_dir . "\\web.config", $server_dir . "\\Foundation\\web.config") or die "Copy failed: $!";
  print "***************************************************************************\n";
}

######################################################################
#
# Routine - connect_to_remote_host
#
#  connect to remote host WMI service
#  params: remote ip (IPv4), user name, user password
#  return: remote host WMI service
#
######################################################################
sub connect_to_remote_host {
    my $remotehostip = shift(@_);
    my $username = shift(@_);
    my $password = shift(@_);
    
    #check remote ip validity (IPv4)
    die "remote IP is invalid: $remotehostip" unless $remotehostip =~ /^\d+\.\d+\.\d+\.\d+$/;
    
    my $locatorObj =Win32::OLE->new("WbemScripting.SWbemLocator") or die "ERROR CREATING OBJ";
    $locatorObj->{Security_}->{impersonationlevel} = 3;
	print "remotehostip - $remotehostip  -  username - $username  -  password - $password\n";
    my $objWMIService = $locatorObj->ConnectServer($remotehostip, "root\\cimv2", $username, $password)
        or die "WMI connection failed.\n", Win32::OLE->LastError;
        
    print "connected to remote host $remotehostip\n";
    return $objWMIService;
}

######################################################################
#
# Routine - load_regex_array
# input - none
# output - none
#
#  List of files to be ignored on purpose
#
######################################################################
sub load_regex_array {
  open (my $rf, $regex_file);

  while (my $line = <$rf>) {
    chomp $line;
    push (@regex_array, $line);
  }

  close $rf;
}

######################################################################
#
# Routine - create_inter_dirs
# input - none
# output - none
#
#  Clears any existing staging dir structure, recreates tree as empty
#
######################################################################
sub create_inter_dirs {
  print "   Deleting $intermediate_dir\n";
  if (-d $intermediate_dir) {
     remove_tree($intermediate_dir);
  }
  if (-d $intermediate_dir) {
    print "ERROR - $intermediate_dir is still present - remove_tree failed - quitting...\n";
    exit(1);
  }
  print "   Creating $add_dir\n";
  create_inter_dir_sub($add_dir);
  print "   Creating $delete_dir\n";
  create_inter_dir_sub($delete_dir);
  print "   Creating) $old_version_dir\n";
  create_inter_dir_sub($old_version_dir);
  print "   Creating $new_version_dir\n";
  create_inter_dir_sub($new_version_dir);
  print "   Creating $shareview_int_config_dir\n";
  create_inter_dir_sub($shareview_int_config_dir);
  print "   Creating $foundation_int_config_dir\n";
  create_inter_dir_sub($foundation_int_config_dir);
  print "   Creating $shareview_int_config_revert_dir\n";
  create_inter_dir_sub($shareview_int_config_revert_dir);
  print "   Creating $foundation_int_config_revert_dir\n";
  create_inter_dir_sub($foundation_int_config_revert_dir);
}
 
######################################################################
#
# Routine - create_inter_dir_sub
# input - dir to create
# output - none
#
#  simple call to make_path with error checking
#
######################################################################
sub create_inter_dir_sub {
  $local_dir = shift;
  if (!-d $local_dir) {
    make_path($local_dir) or die "Could not create $local_dir";
  }
}
 
######################################################################
#
# Routine - verify_inter_dirs
# input - none
# output - none
#
#  Examines each intermediate tree part to make sure it's all there
#
######################################################################
sub verify_inter_dirs {
  verify_inter_dir_sub($intermediate_dir);
  verify_inter_dir_sub($add_dir);
  verify_inter_dir_sub($delete_dir);
  verify_inter_dir_sub($old_version_dir);
  verify_inter_dir_sub($new_version_dir);
  verify_inter_dir_sub($shareview_int_config_dir);
  verify_inter_dir_sub($foundation_int_config_dir);
  verify_inter_dir_sub($shareview_int_config_revert_dir);
  verify_inter_dir_sub($foundation_int_config_revert_dir);
}
 
######################################################################
#
# Routine - verify_inter_dir_sub
# input - dir to verify existance
# output - none
#
#  simple call to make sure individual dir exists - errs if not
#
######################################################################
sub verify_inter_dir_sub {
  $local_dir = shift;
  if (!-d $local_dir) {
    print "Revert action selected, but intermediate dirs ($local_dir) are not all available - quitting.\n";
    exit(1);
  }
}
 
######################################################################
#
# Routine - delete_undesirable_folders
# input - none
# output - none
#
#  Takes directories out of local build tree that should not be
#  copied to destination
#
######################################################################
sub delete_undesirable_folders {
  my $mypath = "$src_dir\\";
 
  print "  Removing " . $mypath . "Foundation\\PrecompiledWeb" . "\n";
  remove_tree($mypath . "Foundation\\PrecompiledWeb");
 
  print "  Removing ShareView config files\n";
  $num1 = unlink glob $mypath . "*config*.*";
  $num2 = unlink glob $mypath . "*.*config*";
 
  print "  Removing ShareView test files\n";
  $num3 = unlink glob $mypath . "*test*.*";
  $num4 = unlink glob $mypath . "*.*test*";
 
  print "  Removing ShareView sports_1_7.csproj file\n";
  $num5 = unlink glob $mypath . "sports_1_7.csproj";
 
  print "  Removing Foundation config files\n";
  $num6 = unlink glob $mypath . "Foundation\\*config*.*";
  $num7 = unlink glob $mypath . "Foundation\\*.*config*";
 
  print "  Removing Foundation website.publishproj file\n";
  $num8 = unlink glob $mypath . "Foundation\\website.publishproj";
 
  print "  Removing Foundation Foundation files\n";
  $num9 = unlink glob $mypath . "Foundation\\*Foundation*.*";
  $num10 = unlink glob $mypath . "Foundation\\*Foundation*.*";
 
  print "  Removing " . $mypath . "Foundation\\.vs" . "\n";
  remove_tree($mypath . "Foundation\\.vs");
 
  print "  Removing " . $mypath . ".vs" . "\n";
  remove_tree($mypath . ".vs");
}
 

######################################################################
#
# Routine - print_deploy_delta_list
# input - none
# output - none
#
#  Displays end-result actions to happen during later deploy - files to be added to. deleted from,
#  and revised in destination location.
#  First section forward check from source to destination to check for adds and new revisions
#  Second section to reverse check from destination to source to check for deletes
#
######################################################################
sub print_deploy_delta_list {
  $run_mode = "add_and_change";
  process_deploy_dir($src_dir);
  print "\n";
  if ($Include_Deletes_and_Not_Tracked eq "Yes") {
    open($not_tracked_filehandle, '>', $not_tracked_file) or die "Could not open file $not_tracked_file' $!";
    $run_mode = "delete";
    process_deploy_dir($server_dir);
    close $not_tracked_filehandle;
  }
}

######################################################################
#
# Routine - process_deploy_dir
# input - root directory to be processed
# output - nothing
#
#  Recursion directory drill-down routine name to be processed for reporting and
#         (optionally) populating intermediate staging area - RECURSIVE!
#  Opens given dir, reads contents, calls process_files for all files, and
#  process_dirs for all dirs
#
######################################################################
sub process_deploy_dir {
  my $path = shift;
  
  if ($path ne "") {
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files = grep { !/^\.{1,2}$/ } readdir (DIR);
    closedir (DIR);
    @files = map { $path . '\\' . $_ } @files;
    for (@files) {
      if ($_ ne ".git") {
        if (-d $_) {
          process_deploy_dir($_);
        } else {
          process_deploy_file($_);
        }
      }
    }
  }
}
 
######################################################################
#
# Routine - process_deploy_file
# input - filename to be processed
# output - none
#
#    computes "to" path for copies and reporting, 
#    if mode ($run_mode setting) is "add_and_change",
#        if file present in build tree, but not on server,
#            reports, and if deploy was selected, copies files to staging add dir
#        if file different in build tree vs server, 
#            reports, if deploy selected,
#               copies existing destination server version to staging "old" dir,
#               and copies build tree version to staging "new" dir
#    otherwise - if file on server, but not build tree,
#        if file tracked in Git,
#            reports, if deploy selected, copies file from server to "delete" staging dir
#        otherwise, reports to "not tracked" file in public dir
#
######################################################################
sub process_deploy_file {
  my $infile = shift;
  my $found_in_regex = "false";
 
  my $newpath = "";
  if ($run_mode eq "add_and_change") {
     ($newpath = $infile) =~ s/\Q$src_dir/$server_dir/;   # $newpath is destination server file path - QA server, or whichever
  } else {
     ($newpath = $infile) =~ s/\Q$server_dir/$src_dir/;   # delete query - for comparing server contents ($infile - QA or whichever) against Git checkout ($newpath, regex subbed from $src_dir)
  }
 
  if ($run_mode eq "add_and_change") {
    if ((-e $infile) && (! -e $newpath)) {
      print "Add - $newpath\n";
      if ($do_deploy eq "true") {  # Part two - copy to intermediate add dir if deploy action was selected
        ($new = $infile) =~ s/\Q$src_dir/$add_dir/;     # swap in new path to intermediate add folder
        @b = fileparse($new);                           # isolate full absolute path for individual file in intermediate
        @c = fileparse($newpath);
        make_path ($b[1], { verbose => 0});             # create that full absolute path
        copy($infile, $b[1]) or die "Copy failed: $!";  # copy file into the intermediate area
      }
    } elsif ((-e $infile) && (-e $newpath)) {
      my $fh1, $fh2;
      open ($fh1, $infile);
      my $sha1 = Digest::SHA1->new;
      $sha1->addfile($fh1);
      $sha1text = $sha1->hexdigest;
      close $fh1;
      open ($fh2, $newpath);
      my $sha2 = Digest::SHA1->new;
      $sha2->addfile($fh2);
      $sha2text = $sha2->hexdigest;
      close $fh2;
      if ($sha1text ne $sha2text) {
        print "New version - $newpath\n";
        if ($do_deploy eq "true") {  # Part two - copy to intermediate change dirs if deploy action was selected
          ($new = $infile) =~ s/\Q$src_dir/$new_version_dir/;     # swap in new path to intermediate add folder
          ($old = $infile) =~ s/\Q$src_dir/$old_version_dir/;
          @new_vers = fileparse($new);                            # isolate full absolute path for individual file in intermediate
          @old_vers = fileparse($old);
          $new_vers_dir = $new_vers[1];
          $old_vers_dir = $old_vers[1];
          make_path ($new_vers_dir, { verbose => 0});             # create that full absolute path
          make_path ($old_vers_dir, { verbose => 0});
          copy($infile, $new_vers_dir) or die "Copy failed: $!";  # copy file into the intermediate area
          copy($newpath, $old_vers_dir) or die "Copy failed: $!";
        }
      }
    }
  } else {
    if ((-e $infile) && (! -e $newpath)) {
      foreach (@regex_array) {               # Does this path contain a key from the ignore list?
        if ($newpath =~ /\Q$_/i) {
          $found_in_regex = "true";
	      last;
        }
      }

      if ($found_in_regex eq "true") {       # It does?  Seriously?  Then don't bother with it.
        return;
	  }

      if (is_file_tracked($newpath) eq "true") {
        print "Delete - $infile\n";
        if ($do_deploy eq "true") {  # Part two - copy to intermediate delete dir if deploy action was selected
          ($delete = $infile) =~ s/\Q$server_dir/$delete_dir/;
          @del_vers = fileparse($delete);
          $del_dir = $del_vers[1];
          make_path ($del_dir, { verbose => 0});
          copy($infile, $del_dir) or die "Copy failed: $!";  # copy to intermediate delete dir
        }
      } else {
        print $not_tracked_filehandle "$newpath\n";
      }
    }
  }
}
 
######################################################################
#
# Routine - is_file_tracked
# input - file to be checked
# output - boolean
#
#     Asks if named file is tracked (corresponding) in Git
#
######################################################################
sub is_file_tracked {
  $local_filename = shift;
  ($filename = $local_filename) =~ s@\Q$src_dir\E\\@ShareView/web/sports_1_7/@;
  $filename =~ s/\\/\//g;
 
  $tracked = `git -C $ENV{"WORKSPACE"} ls-files --error-unmatch \"$filename\" 2> null`;
  chomp($tracked);

  if ($tracked eq $filename) {
    return "true";
  } else {
    return "false"
  }
}
 
######################################################################
#
# Routine - print_revert_delta_list
# input - none
# output - none
#
#   High level call to traverse intermediate staging dirs and run revert
#   operations on findings per functional area
#
######################################################################
sub print_revert_delta_list {
  process_revert_dirs($add_dir, "add");
  process_revert_dirs($delete_dir, "delete");
  process_revert_dirs($old_version_dir, "revert_version");
}
 
######################################################################
#
# Routine - process_revert_dirs
# input - root directory name to be processed - RECURSIVE!
# output - nothing
#
#  Recursion directory drill-down routine for revert operations
#  Opens given dir, reads contents, calls process_files for all files, and
#  process_revert_dirs for all dirs
#
######################################################################
sub process_revert_dirs {
   my $path = shift;
   my $operation = shift;
 
  if ($path ne "") {
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files = grep { !/^\.{1,2}$/ } readdir (DIR);
    closedir (DIR);
    # @files = map { $path . '/' . $_ } @files;
    @files = map { $path . '\\' . $_ } @files;
    for (@files) {
      if ($_ ne ".git") {
        if (-d $_) {
          process_revert_dirs ($_, $operation);
        } else {
          process_revert_file($_, $operation);
        }
      }
    }
  }
}
 
######################################################################
#
# Routine - process_revert_file
# input - filename to be processed
# output - 
# returns - nothing
#
#   Per file (in staging functional area) computes corresponding server
#   filename, and performs appropriate revert action
#
######################################################################
sub process_revert_file {
  my $infile = shift;
  my $operation = shift;
 
  if ($operation eq "add") {
    ($new = $infile) =~ s/\Q$add_dir/$server_dir/;
    print "Removing added file $new\n";
    unlink $new;
  } elsif ($operation eq "delete") {
    ($new = $infile) =~ s/\Q$delete_dir/$server_dir/;
    print "Restoring deleted file $new\n";
    copy($infile, $new);
  } elsif ($operation eq "revert_version") {
    ($new = $infile) =~ s/\Q$old_version_dir/$server_dir/;
    print "Restoring old file version of file $new\n";
    copy($infile, $new);
  } else {
    print "No operation - $infile\n";
  }
}
 
######################################################################
#
# Routine - deploy_from_int_to_dest
# input -
# output -
# returns -
#
#    Highest level calls for deploy actions from intermediate
#    staging area to server - actions for both deploy and revert
#
######################################################################
sub deploy_from_int_to_dest {
  my $action = shift;
  if ($action eq "Deploy") {
    recurse_staging_subdir($add_dir, "add");
    recurse_staging_subdir($delete_dir, "delete");
    recurse_staging_subdir($new_version_dir, "change");
  } elsif ($action eq "Revert") {
    print "Revert stuff\n";
    recurse_staging_subdir($add_dir, "remove_add");
    recurse_staging_subdir($delete_dir, "restore_delete");
    recurse_staging_subdir($old_version_dir, "restore_change");
  }
}
 
######################################################################
#
# Routine - recurse_staging_subdir
# input -
# output -
# returns -
#
#    Calls for broken down action types during transfer from
#    staging area to server (or revert)
#
######################################################################
sub recurse_staging_subdir {
  my $entry = shift;
  my $do_mode = shift;
 
  opendir (DIR, $entry);
  my @files = grep { !/^\.{1,2}$/ } readdir (DIR);
  closedir (DIR);
 
  @files = map { $entry . '\\' . $_ } @files;
  for (@files) {
     if (-d $_) {
        recurse_staging_subdir ($_, $do_mode);
     } else {
        if ($do_mode eq "add") {
           process_add($_);
        } elsif ($do_mode eq "delete") {
           process_delete($_);
        } elsif ($do_mode eq "change") {
           process_change($_);
        } elsif ($do_mode eq "remove_add") {
           process_remove_add($_);
        } elsif ($do_mode eq "restore_delete") {
           process_restore_delete($_);
        } elsif ($do_mode eq "restore_change") {
           process_restore_change($_);
        }
     }
  }
}
 
######################################################################
#
# Routine - process_add
# input - file to process
# output -
# returns -
#
#    Action to copy new file from staging area to server (and make dir if neccessary)
#
######################################################################
sub process_add {
  my $infile = shift;
 
  ($new = $infile) =~ s/\Q$add_dir/$server_dir/;  # swap in new path to intermediate add folder
  @b = fileparse($new);                           # isolate full absolute path for individual file in intermediate
  if (! -d $b[1]) {
    print "making path $b[1]\n";
    make_path ($b[1], { verbose => 0});          # create that full absolute path
  }
 
  print "Adding from $infile to $new\n";
  copy($infile, $new) or die "Copy failed: $!";
}
 
######################################################################
#
# Routine - process_delete
# input - file to process
# output -
# returns -
#
#    Action to remove named file in staging area from server
#
######################################################################
sub process_delete {
  my $infile = shift;
 
  ($delete = $infile) =~ s/\Q$delete_dir/$server_dir/;     # swap in new path to intermediate add folder
  @b = fileparse($delete);                                 # isolate full absolute path for individual file in intermediate
  print "Deleting $delete\n";
  unlink $delete;
}
 
######################################################################
#
# Routine - process_change
# input - file to process
# output -
# returns -
#
#    Action to copy new revision of file from staging area to server
#
######################################################################
sub process_change {
  my $infile = shift;
 
  ($change = $infile) =~ s/\Q$new_version_dir/$server_dir/;     # swap in new path to intermediate add folder
  @b = fileparse($change);                           # isolate full absolute path for individual file in intermediate
  print "Copying changed file: $infile to $change\n";
  copy($infile, $change) or die "Copy failed: $!";
}
 
######################################################################
#
# Routine - process_remove_add
# input - file to process
# output -
# returns -
#
#    Action to remove file named in staging area from server
#
######################################################################
sub process_remove_add {
  my $infile = shift;
 
  ($delete = $infile) =~ s/\Q$add_dir/$server_dir/;     # swap in new path to intermediate add folder
  @b = fileparse($delete);                              # isolate full absolute path for individual file in intermediate
  print "Removing formerly added file: $delete\n";
  unlink $delete;
}
 
######################################################################
#
# Routine - process_restore_delete
# input - file to process
# output -
# returns -
#
#  Action to restore file named in staging area to server
#
######################################################################
sub process_restore_delete {
  my $infile = shift;
 
  ($restore = $infile) =~ s/\Q$delete_dir/$server_dir/;     # swap in new path to intermediate add folder
  @b = fileparse($restore);                                # isolate full absolute path for individual file in intermediate
  print "Restoring from $infile to formerly deleted: $restore\n";
  copy($infile, $restore) or die "Copy failed: $!";
}
 
######################################################################
#
# Routine - process_restore_change
# input - file to process
# output -
# returns -
#
#    Action to copy old revision of file from staging area to server
#
######################################################################
sub process_restore_change {
  my $infile = shift;
 
  ($change = $infile) =~ s/\Q$old_version_dir/$server_dir/;     # swap in new path to intermediate add folder
  @b = fileparse($change);                           # isolate full absolute path for individual file in intermediate
  print "Restoring formerly changed file: $infile to $change\n";
  copy($infile, $change) or die "Copy failed: $!";
}
