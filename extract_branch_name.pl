use JSON;

my $PR_num = $ENV{"Pull_Request_Number"};
my @results = `curl -L -D- -X GET -H "Authorization: Basic amFtZXMuaGVua0BhZmZpbml0eS1zcG9ydHMuY29tOkVhZ2xlYnlGRUQyMDE4IQ==" -H "Content-Type: application/json"  https://bitbucket.org/!api/2.0/repositories/tshqdevelopment/affinity/pullrequests/$PR_num`;

for my $result (@results) {
   if ($result =~ /^{/) {
      $final = from_json( $result );
   }
}

$branch = $final->{source}->{branch}->{name};

if ($branch eq "") {
    print "There is no branch associated with pull request $PR_num - Please investigate - quitting.\n";
	exit(1);
} else {
   print "\n******************************************************\n";
   print "Branch name associated with $PR_num is $branch\n";
   $file = $ENV{"WORKSPACE"} . "\\env.properties";
   open(FH, '>', $file) or die $!;
   print FH "branch=origin/$branch\n";
   close FH;
}
