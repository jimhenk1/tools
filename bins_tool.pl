#!/usr/bin/perl
######################################################################
#
# Name - bins_tool.pl
# Author - Jim Henk
# Date - 7/23/2018
#
#
######################################################################

my $input_dir = shift;
my $stand_alone = shift;
my $num = 0;

print "Checking tree for DROP and/or TRUNCATE statements\n";
print "*************************************************\n";

if ($ENV{"Branch"} eq "origin/develop") {
   $nope = "True";
   $nope_branch = "origin/develop";
} elsif ($ENV{"Branch"} eq "origin/master") {
   $nope = "True";
   $nope_branch = "origin/master";
}

if ($nope eq "True") {
   print "***** Branch is $nope_branch - Database tree is not applicable.  Quitting normally.\n";
   exit;
}

process_dir($input_dir);

if ($num == 0) {
   print "\n***** None found.  Proceeding.\n";
} else {
   print "\n***** ERROR - $num DROP or TRUNCATE statement(s) found in tree.\n";
   if (($ENV{"Override_DB_Standards"} eq "Override_DB_Standards") || ($stand_alone eq "-y")) {
      print "Proceeding.\n";
      exit;
   } else {
      print "Exiting process.\n";
      exit(1);
   }
}

######################################################################
#
# Routine - process_deploy_dirs
# input - root directory name to be processed - RECURSIVE!
# output - nothing
#
#  Recursion directory drill-down routine
#  Opens given dir, reads contents, calls process_files for all files, and
#  process_dirs for all dirs
#
######################################################################
sub process_dir {
  my $path = shift;
   
  if ($path ne "") {
    opendir (DIR, $path) or die "Unable to open $path: $!";
    my @files = grep { !/^\.{1,2}$/ } readdir (DIR);
    closedir (DIR);
    # @files = map { $path . '/' . $_ } @files;
    @files = map { $path . '\\' . $_ } @files;
    for (@files) {
      if (-d $_) {
        process_dir ($_);
      } else {
	    if (/\.sql$/) {
		  process_file($_);
		}
      }
	}
  }
}

######################################################################
#
# Routine - process_file
# input - filename to be processed
# output - same file, but only changed if RCS words found and edited
# returns - nothing
#
#
######################################################################
sub process_file {
  my $infile = shift;
  my $display_status = "no";
  my $line_num = 1;

  open ($file1, $infile);
  while ($row = <$file1>) {
    chomp $row;
	if ($row =~ /DROP|TRUNCATE/i) {
	  if ($display_status eq "no") {
	    print "\n$infile\n";
		$display_status = "yes";
	  }
	  print "****  $line_num:  $row\n";
	  $num = $num + 1;
	}
	$line_num = $line_num + 1;
  }
  close $file1;
}
