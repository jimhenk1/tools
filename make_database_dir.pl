use File::Basename;

my $conflict_flag = "false";
my $file_conflict = "false";
my $conflict_count = 0;
my $row;
my $index = 1;
my $in_db_file = "false";
my $pull_request = $ENV{"Pull_Request_number"};
my $nope = "";
my $nope_branch = "";

print "Database directory creation\n";
print "***************************\n";


if ($ENV{"Branch"} eq "origin/develop") {
   $nope = "True";
   $nope_branch = "origin/develop";
} elsif ($ENV{"Branch"} eq "origin/master") {
   $nope = "True";
   $nope_branch = "origin/master";
}

if ($nope eq "True") {
   print "***** Branch is $nope_branch - Database tree is not applicable.  Quitting.\n";
   exit;
}

print "Creating database directory for sql scripts from pull request $pull_request.\n";

my @results = `curl -L -D- -X GET -H "Authorization: Basic amFtZXMuaGVua0BhZmZpbml0eS1zcG9ydHMuY29tOkVhZ2xlYnlGRUQyMDE4IQ==" -H "Content-Type: application/json"  https://bitbucket.org/!api/2.0/repositories/tshqdevelopment/affinity/pullrequests/$pull_request/diff`;
my $size = @results;

my $database_root = "c:\\users\\public\\Documents\\database";

my $parse_filename;
my $directories;
my $suffix;

if (-d $database_root) {
  print "Deleting and re-creating database root - $database_root\n";
  `rmdir /s /q $database_root`;
  `mkdir $database_root`;
}

for( $a = 0; $a <= $size; $a = $a + 1 ){
  $row = $results[$a];
  chomp $row;
  $row =~ s{/}{\\}g;
  
  if ($row =~ /^diff --git a\\([\w|\-|\.|\s|\\]*)\sb\\/) {  # filename row?
    $filename = $1;
    if ($filename =~ /^Database/) {                         # does it begin with Database?
	  $in_db_file = "true";                                 # start processing set for this file
	} else {
	  $in_db_file = "false";                                # nope - must be Shareview or something...
	}
	if ($in_db_file eq "true") {
	  if (($filename !~ /\.sqlproj$/) && ($results[$a + 1] !~ /deleted\sfile\smode\s100644$/)) {  # peek ahead one element in array looking for delete flag
	                                                                                              # also, ignore *.sqlproj file(s)
	    $infile = $ENV{"WORKSPACE"} . "\\" . $filename;
		($parse_filename, $directories, $suffix) = fileparse($filename);
		$out_dir = $database_root . "\\" . $directories;
		$out_file = $out_dir . $parse_filename;
		if (! -d $out_dir) {
		  print "Creating $out_dir\n";
		  `mkdir "$out_dir"`;
		}
		print "copy:   $infile \n";
		print "        $out_file\n\n";
		`copy "$infile" "$out_file"`;
		$index = $index + 1;
		if (! -e $infile) {
		  print " XXXXXXX Not exists - ";
		  print "\n$row\n";
		}
	  }
      $file_conflict = "false"
	  
    }
  } elsif ($row =~ /<<<<<<</) {
    if ($in_db_file eq "true") {
      if ($file_conflict ne "true") {
        print "   >>>>>> CONFLICT!";
        $file_conflict = "true";
        $conflict_flag = "true";
        $conflict_count++;
	  }
	}
  }
}

print "\n\n";

if ($conflict_flag eq "true") {
  print "******* ERROR!  There were $conflict_count unresolved conflicts.  Quitting.\n";
  exit(1);
}
