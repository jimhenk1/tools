############################################################################################
#                                                                                          #
#  CreateHotFixFolders.pl                                                                  #
#                                                                                          # 
#    To create architecturally correct deploy and rollback folders for hotfixes, etc.      #
#    Folders created locally - does *NOT* write to network target folder                   #
#                                                                                          #
#    Reads developer-supplied list of files to be deployed                                 #
#    Creates Deploy folder of new files, and Rollback folder of                            #
#      original files already present.                                                     #
#                                                                                          #
#       Copyright 2018 - Affinity Sports                                                   #
#       Jim Henk                                                                           #
#       7/19/2018                                                                          #
#                                                                                          #
############################################################################################

# new line just so an initial pull request can be generated.

use File::Basename;
use File::Path qw(make_path remove_tree);
use File::Copy;
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);
use File::Path qw(make_path remove_tree);
use Cwd;
use Switch;
use String::Util qw(trim);
use Switch;

my @c = ();

my $dir = getcwd . "\\";
$dir =~ s{/}{\\}g;

my $config_file    = $dir . "CreateHotFixFoldersConfig.pl";   # File of authentications
my $bins_tool      = $dir . "bins_tool.pl";

my $rollback_root  = $dir . "rollback\\";          # Base rollback folder
my $topath         = $dir . "deploy\\";            # Base deploy folder
my $database_root  = $dir . "database\\";          # Base database folder
my $rollback_drive = "P:\\"; 
my $filelist_file  = "maketree.txt";               # branch file list manually pulled from pull request

my $errors = 0;                                    # Mapped network target folder
my $rollback_warn = 0;

my $dirty = "";
my $conflict_flag = "false";

# Specific path variables get populated out of this config file, so search that file for vars, too.
open CONFIG, "$config_file" or die "Program stopping, couldn't open the configuration file '$config_file'.\n";
my $config = join "", <CONFIG>;
close CONFIG;
eval $config;
if ($@) {
   print "**** Error - couldn't interpret the configuration file ($config_file) that was given.\nError details follow: $@\n";
}

system 'cls';
print "\n   Merge Deploy Procedure\n\n";

print "Destination server? (QA, QA1, QA2, STAGE, CS1, CS2 - contents will not be modified: ";
$input = <STDIN>;
chomp($input);

print "Branch name?: ";
$branch = <STDIN>;
$branch = trim($branch);
chomp($branch);

print "Pull request number?: ";
$pull_request = <STDIN>;
$pull_request = trim($pull_request);
chomp($pull_request);

$rollback_path  = $rollback_root . $input . "\\" . $branch . "\\";  # Filled out rollback folder

switch($input) {                                                    # Which server selected? Get server and username from list from config file
  case "QA" {
    $server = $QAserver;
    $user = $QAuser;
  }
  case "QA1" {
    $server = $QA1server;
    $user = $QA1user;
  }
  case "QA2" {
    $server = $QA2server;
    $user = $QA2user;
  }
  case "STAGE" {
    $server = $STAGEserver;
    $user = $STAGEuser;
  }
  case "CS1" {
    $server = $CS1server;
    $user = $CS1user;
  }
  case "CS2" {
    $server = $CS2server;
    $user = $CS2user;
  } else {
    print "**** Error - server choice not valid. Quitting...\n";
    exit(1);
  }
}

# ************************************************************************************************************

#    GIT Operations

print "\n\n\nFirst things first...\n";
print "Retrieving $branch into workspace, and checking need for forward integration.\n\n";
chdir $frompath;

print "fetching origin...\n";
$return = `git fetch --all 2> null`;
chomp($return);
if ($return eq "") {
  $return = "OK";
}
print "#### - fetch - $return\n";
print "********************************************\n";

print "checking for the existence of $branch on origin...\n";
$return = `git rev-parse --verify --quiet origin/$branch  2> null`;
chomp($return);
if ($return eq "") {
  print "Could not find branch $branch - perhaps you left out the preceeding hotfix/ feature/ or release/ ?\n";
  exit(1);
} else {
  $return = "OK";
}
print "#### - rev-parse - $return\n";
print "********************************************\n";


print "Attempting to checkout $branch...\n";
$checkout = `git checkout $branch 2> null`;
chomp($checkout);
 if ($checkout eq "") {
  print "Could not find branch $branch - perhaps you left out the preceeding hotfix/ feature/ or release/ ?\n";
  exit(1);
}
print "#### - checkout - $checkout\n";
print "********************************************\n";

print "Pulling $branch from origin...\n";
$pull = `git pull origin $branch 2> null`;
chomp($pull);
print "#### - pull - $pull\n";
print "********************************************\n";

print "Checking to see if remotes/origin/master (or develop - depending on hotfix or feature...) appears in list of ummerged branches...\n";
if ($branch =~ /^release\//i) {
  print "\n**** SPECIAL NOTE - Release branch - no forward integration ****\n";
  print "**** Do you have confirmation from the owner that the release branch is ready / or has been finalized? (Y/N case-sensitive)  ";
  $release_answer = <STDIN>;
  chomp ($release_answer);
  if ($release_answer ne "Y") {
    print "Please complete that task, and re-run.  Quitting...\n";
    exit(1);
  }
} else {
  @ret_ary = `git branch -a --no-merge 2> null`;
  foreach $loop (@ret_ary) {
    chomp($loop);
    $loop =~ s/^\s*//;
    if ($branch =~ /^feature\//i) {
      if ($loop eq "remotes/origin/develop") {
        print "#### ERROR : Found remotes/origin/develop in non-merged list.  Needs forward integration from develop.  Override? (Y/N)";
		$override_answer = <STDIN>;
		chomp ($override_answer);
		if ($override_answer ne "Y") {
			exit(1);
		}
      }
    } elsif ($branch =~ /^hotfix\//i) {
      if ($loop eq "remotes/origin/master") {
        print "#### ERROR : Found remotes/origin/master in non-merged list.  Needs forward integration from master.  Override? (Y/N)";
		$override_answer = <STDIN>;
		chomp ($override_answer);
		if ($override_answer ne "Y") {
		  exit(1);
		}
	  }
    }
  }  # end foreach
}
print "#### - merge check - OK\n";

print "********************************************\n";
print "All clear!\n\n";

# ************************************************************************************************************
 
print "Will:\n";
print "  1) delete any existing mapping of P:\n";
print "  2) attempt to attach to server $server as user $user\n";
print "  3) attempt to map server as P:\n";
 `net use /delete p: > nul 2>&1`;                 # delete any existing mapping for drive letter P:

print "\nEnter password, please: ";

`net use p: $server * /USER:$user`;  # > nul 2>&1`;    # map P: to chosen network target folder

if (-d "p:\\") {
  print "Mapping successful.\n";
} else {
  print "Mapping not successful - exiting.\n";
  exit(1);
}

print "\nDeleting $topath" . "...\n";
remove_tree($topath);                             # start again on deploy folder
print "Deleting $rollback_path" . "...\n";
remove_tree($rollback_path);                      # start again on rollback folder
print "Deleting $database_root" . "...\n";
remove_tree($database_root);                      # start again on database folder

my @results = `curl -L -D- -X GET -H "Authorization: Basic amFtZXMuaGVua0BhZmZpbml0eS1zcG9ydHMuY29tOkVhZ2xlYnlGRUQyMDE4IQ==" -H "Content-Type: application/json"  https://bitbucket.org/!api/2.0/repositories/tshqdevelopment/affinity/pullrequests/$pull_request/diff`;
my $size = @results;

for( $a = 0; $a <= $size; $a = $a + 1 ){
  $row = $results[$a];
  chomp $row;
  $row =~ s{/}{\\}g;
  
  if ($row =~ /^diff --git a\\([\w|\-|\.|\s|\\]*)\sb\\/) {  # filename row?
    $filename = $1;
	print "\n*** $filename\n";

    @c = fileparse($filename);                      # isolate path from filename into array, build vars
    $from = $frompath . $filename;                  # local build folder filepath
    $todir = $topath . $c[1];                       # deploy folder full folder path
    $to = $topath . $filename;                      # deploy folder full file path
  
	if ($filename =~ /^Database/) {                         # does it begin with Database?
      $in_db_file = "true";                                 # start processing set for this file
	  $file_conflict = "false";
 	} else {
	  $in_db_file = "false";                                # nope - must be ShareView or something...
	}

	if ($filename =~ /^ShareView/) {                        # does it begin with Shareview?
      $in_sv_file = "true";                                 # start processing set for this file
	  $file_conflict = "false";
 	} else {
	  $in_sv_file = "false";                                # nope - must be something else...
	}

    if ($in_db_file eq "true") {
      if (($filename !~ /\.sqlproj$/) && ($filename !~ /\.csproj$/) && ($results[$a + 1] !~ /deleted\sfile\smode\s100644$/)) {  # peek ahead one element in array looking for delete flag
	                                                                                              # also, ignore *.sqlproj file(s)
		($parse_filename, $directories, $suffix) = fileparse($from);
		$out_dir = $database_root . $c[1];
		$out_file = $out_dir . $c[0];
        if (! -e $from) {
		  print " XXXXXXX Not exists - ";
		  print "\n$from  ";
		} else {
		  if (! -d $out_dir) {
		    print "Creating $out_dir\n";
		    `mkdir "$out_dir"`;
		  }
		  print "copy:   $from \n";
		  print "        $out_file  ";
		  `copy "$from" "$out_file"`;
		  $index = $index + 1;
        }
      }
    } elsif ($in_sv_file eq "true") {
      if ($filename =~ /web\.config/i) {                   #   section for web.config
        print "\n";
	    print "########\n";
	    print "########\n";
        print "########   WARNING - filename - $filename - contains web.config - continuing...\n";
	    print "########\n";
	    print "########\n";
	    $dirty = "Yes";
      }
	
      if ($filename =~ /ShareView\\web\\sports_1_7\\Foundation\\packages\.config/i) {   # Foundation\packages.config?  Do nuget operations
        $packages = $frompath . "ShareView\\web\\sports_1_7\\Foundation\\packages";
        $bin = $topath . "ShareView\\web\\sports_1_7\\Foundation\\bin";
        $file = $from;
	
        do_nuget($packages, $bin, $file);
      }

      if ($filename =~ /ShareView\\web\\sports_1_7\\packages\.config/i) {               # root packages.config?  Do nuget operations
        $packages = $frompath . "ShareView\\web\\sports_1_7\\packages";
        $bin = $topath . "ShareView\\web\\sports_1_7\\bin";
        $file = $from;
	
        if (! -d $packages) {
          `mkdir $packages`;
        }
        do_nuget($packages, $bin, $file);
      }
	
      $rollback_src = $rollback_drive . $filename;                   # (raw) network target deploy file full path
      $rollback_src =~ s/ShareView\\web\\sports_1_7\\//;             # screen out extra pathing from deploy file
      $rollback_path_sub = $rollback_path . $c[1];                   # build rollback folder full file path
	  
      if (! -d $todir) {
        print "\nmkdir " . "\"" . $todir . "\"\n";                     # Deploy folder
        `mkdir "$todir"`;
      }
      print "copy \"" . $from . "\"" . " \"" . $to . "\"\n";
      if (-e $from) {                                                # do copy unless file not found in local build tree
        `copy "$from" "$to"`;
      } else {
        print " ***** - ERROR - $from does not exist.  Quitting.\n";                  # Oops!
        exit(1);
      }

      if (! -d $rollback_path_sub) {
        print "\nmkdir " . "\"" . $rollback_path_sub . "\"\n";         # Rollback folder creation and file copy
        `mkdir "$rollback_path_sub"`;
      }
      $dest = $rollback_path_sub . $c[0];
      print "copy \"" . $rollback_src . "\"" . " \"" . $dest . "\"\n";
      if (-e $rollback_src) {
        `copy "$rollback_src" "$dest"`;
      } else {
        print " ***** - WARNING - $rollback_src does not exist.  Ignoring.\n";   # Oops!
        $rollback_warn = $rollback_warn + 1;
      }
	}
	
	print "\n";
  } elsif ($row =~ /<<<<<<</) {
    if ($file_conflict ne "true") {
      print "   >>>>>> CONFLICT!";
      $file_conflict = "true";
      $conflict_flag = "true";
      $conflict_count++;
    }
  } # else {
	 # print "\n***** ERROR - $row ignored - process continued.\n";  # Not Shareview and not Database - Ignore.
	 # $errors = $errors + 1;
  # }

  
  # print "\n######################################################################\n";
}

print "\n***** $errors non-ShareView or Database path error(s) found - Ignored.\n";
print "\n***** $rollback_warn input file(s) not found on target environment - Ignored.\n";

if ($dirty eq "Yes") {              # At tail, a prominent warning that a web.config was found somewhere in the file list.  Evaluate carefully!
    print "\n";
	print "########\n";
	print "########\n";
    print "########   WARNING - web.config detected - evaluate...\n";
	print "########\n";
	print "########\n\n";
}

print "\n######################################################################\n";
$check = `perl $bins_tool $database_root -y`;     # Tool to evaluate files in Database tree for presence of DROP or TRUNCATE keywords
print "\n$check\n";
print "\n######################################################################\n";   # Bye!

sub do_nuget {
    $local_packages = shift;
	$local_bin = shift;
	$local_file = shift;
	
	if (! -d $local_packages) {
	   print "$local_packages does not exist - creating\n";
	   `mkdir $local_packages`;
	} else {
	   if (! -d $local_bin) {
	     print "$local_bin does not exist - creating\n";
	     `mkdir $local_bin`;
       }
	}
	
	print "Running nuget operation for $local_bin.\n";
	print "**** cmd - nuget install -OutputDir $local_packages $local_file\n";
	$nugetoutput = `nuget install -OutputDir $local_packages $local_file 2> null`;
	chomp ($nugetoutput);
	print "$nugetoutput\n\n";
	print "Copying files from $local_packages\n";
	print "                to $local_bin\n";
	my $num_of_files_and_dirs = dircopy($local_packages, $local_bin);
	print "$num_of_files_and_dirs files copied.\n";
}