$branch = "hotfix/DS-258";
$frompath       = "C:\\Users\\jhenk\\Documents\\affinity\\";  # SourceTree checkout folder

chdir $frompath;

print "fetching origin...\n";
$return = `git fetch --all 2> nul`;
chomp($return);
if ($return eq "") {
   $return = "OK";
}

print "#### - fetch - $return\n";
print "********************************************\n";

print "checking for the existence of $branch on origin...\n";
$return = `git rev-parse --verify --quiet origin/$branch  2> nul`;
chomp($return);
if ($return eq "") {
   print "Could not find branch $branch - perhaps you left out the preceeding hotfix/ or feature/?\n";
   exit(1);
} else {
   $return = "OK";
}
print "#### - rev-parse - $return\n";
print "********************************************\n";

if ($branch =~ /^hotfix\//i) {
   print "Attempting to checkout master...\n";
   $checkout = `git checkout master 2> nul`;
   chomp($checkout);
   if ($checkout eq "") {
      print "Could not find branch master - Weird, huh/?\n";
      exit(1);
   }
   print "#### - checkout - $checkout\n";
   print "********************************************\n";

   print "Pulling master from origin...\n";
   $pull = `git pull origin 2> nul`;
   chomp($pull);
   print "#### - pull - $pull\n";
} elsif ($branch =~ /^feature\//i) {
   print "Attempting to checkout develop...\n";
   $checkout = `git checkout develop 2> nul`;
   chomp($checkout);
   if ($checkout eq "") {
      print "Could not find branch develop - Weird, huh/?\n";
      exit(1);
   }
   print "#### - checkout - $checkout\n";
   print "********************************************\n";

   print "Pulling develop from origin...\n";
   $pull = `git pull origin 2> nul`;
   chomp($pull);
   print "#### - pull - $pull\n";
}

print "********************************************\n";
print "Attempting to checkout $branch...\n";
$checkout2 = `git checkout $branch`;
chomp($checkout2);
# if ($checkout2 eq "") {
#   print "Could not find branch $branch - perhaps you left out the preceeding hotfix/ or feature/?\n";
#   exit(1);
# }
print "#### - checkout - $checkout2\n";
print "********************************************\n";

print "Pulling $branch from origin...\n";
$pull = `git pull origin $branch 2> nul`;
chomp($pull);
print "#### - pull - $pull\n";
print "********************************************\n";


print "Checking to see if remotes/origin/master or develop (depending...) appears in list of ummerged branches...\n";
@ret_ary = `git branch -a --no-merge 2> nul`;
foreach $loop (@ret_ary) {
   chomp($loop);
   $loop =~ s/^\s*//;
   if ($branch =~ /^feature\//i) {
      if ($loop eq "remotes/origin/develop") {
         print "#### Found remotes/origin/develop in non-merged list.  Needs forward integration from develop.  Continuing...\n";
         merge_it();
      }
   } elsif ($branch =~ /^hotfix\//i) {
      if ($loop eq "remotes/origin/master") {
         print "#### Found remotes/origin/master in non-merged list.  Needs forward integration from master.  Continuing...\n";
         merge_it();
      }
   }
}
print "#### - merge check - OK\n";


sub merge_it {
   if ($branch =~ /^hotfix\//i) {
      print "Merging master into $branch...\n";
      $merge = `git merge master 2> nul`;
      chomp($merge);
      print "#### - merge - $merge\n";
      if ($merge =~ /CONFLICT/) {
         print "\n**** ERROR - Conflict detected - quitting...\n";
         exit(1);
      }
   } elsif ($branch =~ /^feature\//i) {
      print "Merging develop into $branch...\n";
      $merge = `git merge develop 2> nul`;
      chomp($merge);
      print "#### - merge - $merge\n";
      if ($merge =~ /CONFLICT/) {
         print "\n**** ERROR - Conflict detected - quitting...\n";
         exit(1);
      }
   }

   print "********************************************\n";
   print "No conflicts detected.\n\n";
 
   print "********************************************\n";
   print "Merge completed.  Okay to push to origin? (Y/N case-sensitive)  ";
   $okay_to_push_to_origin_answer = <STDIN>;
   chomp ($okay_to_push_to_origin_answer);
   if ($okay_to_push_to_origin_answer ne "Y") {
      print "Quitting before push could take place.  Bye!...\n";
      exit(1);
   } else {
      $push = `git push origin 2> nul`;
      print "push - $push\n";
    }

    print "********************************************\n";
    print "Job Complete.\n\n";
}
