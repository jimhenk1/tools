use Cwd;
use Win32::NetResource qw(:DEFAULT GetSharedResources GetError);
use File::Basename;
use File::Path qw(make_path remove_tree);
use File::Copy;
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);
use File::Path qw(make_path remove_tree);
use Switch;

use constant wbemFlagReturnImmediately => 0x10;
use constant wbemFlagForwardOnly => 0x20;
 
# temporary re-mapping of array indices until Git Shareview/Configs matches names in Environments Confluence page
my %ip_array = (
  'QA'        => "192.168.4.8",
  'QA1'       => "192.168.97.11",
  'QA2'       => "192.168.97.7",
  'STAGE'    => "192.168.14.12",
  'CS1'    => "192.168.42.7",
  'CS2'    => "192.168.96.5",
);

my %dest_array = (
  'QA'        => "web\\sports_1_7",
  'QA1'       => "WwwRoot\\adc\\sports.affinitydev.com\\site",
  'QA2'       => "WwwRoot\\adc\\sports.affinitydev.com\\site",
  'STAGE'    => "SPTSWEB",
  'CS1'    => "WwwRoot\\adc\\sports.affinitydev.com\\site",
  'CS2'    => "WwwRoot\\adc\\sports.affinitydev.com\\site",
);

my %acctname_array = (
  'QA'        => $ENV{"LDAP_username"},
  'QA1'       => "localadmin",
  'QA2'       => "ciadmin",
  'STAGE'    => $ENV{"LDAP_username"},
  'CS1'    => "dmzadmin",
  'CS2'    => "dmzadmin",
);

my %password_array = (
  'QA'        => $ENV{"LDAP_password"},
  'QA1'       => "achinFP88",
  'QA2'       => "Za4>AjHy4d",
  'STAGE'    => $ENV{"LDAP_password"},
  'CS1'    => "achinFP8",
  'CS2'    => "achinFP8",
);
 
my $server_dir = "";
my $remote = "";
my $ip = "";
my $username = "";
my $password = "";
my $frompath = "C:\\Jenkins\\MergeDeploy\\";  # Affinity checkout folder
my $shareview_files = 0;
my $database_files = 0;
my $other_files = 0;

# Where exactly are we deploying *to* ?  Is it a local dev folder, or a remote server?
my $server_environment = $ENV{"Destination_Server"};

switch($server_environment) {
  case "Local" {
    $server_dir = "C:\\Workspaces\\Local";
	$remote = "false";
  }
  case "LocalQA2" {
    $server_dir = "C:\\Workspaces\\QA2";
	$remote = "false";
  } else {
    $ip = $ip_array{$server_environment};
    $server_dir = "\\\\" . $ip . "\\" . $dest_array{$server_environment};
    $username = $acctname_array{$server_environment};
    $password = $password_array{$server_environment};
	$remote = "true";
  }
}

my @web_config_file_ary = ();
my @packages_config_ary = ();
my $pull_request = $ENV{"Pull_Request_Number"};

my @c =();

my $dir = "C:\\Users\\Public\\Documents\\";

my $branch = $ENV{"branch"};
$branch =~ s{/}{\\}g;

my $bins_tool      = $dir . "bins_tool.pl";

my $rollback_root  = $dir . "rollback\\";          # Base rollback folder
my $topath         = $dir . "deploy\\";            # Base deploy folder
my $database_root  = $dir . "database\\";          # Base database folder
my $rollback_path  = $rollback_root . $server_environment . "\\" . $branch . "\\";  # Filled out rollback folder

print "Generating file list...\n";
my @results = `curl -L -D- -X GET -H "Authorization: Basic amFtZXMuaGVua0BhZmZpbml0eS1zcG9ydHMuY29tOkVhZ2xlYnlGRUQyMDE4IQ==" -H "Content-Type: application/json"  https://bitbucket.org/!api/2.0/repositories/tshqdevelopment/affinity/pullrequests/$pull_request/diff`;

print "\nChecking for web.configs and package.configs in list.\n";
my $size = @results;

for( $a = 0; $a <= $size; $a = $a + 1 ) {
   $row = $results[$a];
   chomp $row;
   $row =~ s{/}{\\}g;
  
   if ($row =~ /^diff --git a\\([\w|\-|\.|\s|\\]*)\sb\\/) {  # filename row?
      $filename = $1;
	  print "*** $filename\n";

      if ($filename =~ /^ShareView/) {                        # does it begin with Shareview?
         if ($filename =~ /web\.config/i) {                    # web.config?
            print "\n";
	        print "########\n";
	        print "########\n";
            print "########   WARNING - filename - $filename - contains web.config - continuing...\n";
	        print "########\n";
	        print "########\n";
	        $web_config_present = "True";
		    push @web_config_file_ary, $filename;
         }

         if ($filename =~ /packages\.config/i) {
            $packages_config_present = "true";
		    push @packages_config_ary, $filename;
         }
      }
   }
}

if ($packages_config_present eq "True") {
   print "*** Alert: packages.config files are present in this file list.   Continuing...\n";
   foreach my $i (0 .. $#packages_config_ary) {
      print  "   $packages_config_ary[$i]";
   }
   print "Nuget operations have been executed. (see earlier in this log)  Proceeding...\n";
}

if ($web_config_present eq "True") {
   print "*** Warning: web.config files are present in this file list.\n";
   foreach my $i (0 .. $#web_config_file_ary) {
      print  "   $web_config_file_ary[$i]";
   }
   if ($ENV{"Web.config_permissions_granted"} eq "Exit if no permission") {
      print "No permission granted to proceed.  Now quitting...\n";
	  exit(1);
   } else {
      print "Permission to proceed granted.  Proceeding...\n";
   }
}

if (($packages_config_present ne "True") && ($web_config_present ne "True")) {
   print "All clear...\n";
}

print "\n######################################################################\n";
  
# Connect to remote host if selected server is a remote server
if ($remote eq "true") {
   my %RemoteShare = (
      'RemoteName' => "\\\\" . $ip,
   );
   print "\nConnecting to remote: $server_dir... ";
   if (Win32::NetResource::AddConnection(\%RemoteShare, $password, $username, 1)) {
      print "OK\n";
      print "\nVerifying $server_dir...\n";
      if (! -d $server_dir) {
	     print "Hmmm... Cannot find it.  Quitting...\n";
	     exit(1);
      } else {
	     print "Verification successful.\n";
      }
   } else {
      my $err;
      GetError($err);
      print "FAIL...\n";
      print "$err\n";
      exit(1);
   }
} else {
   print "Verifying $server_dir... ";
   if (-d $server_dir) {
      print "OK\n";
   } else {
      print "FAIL - quitting\n";
      exit(1);
   }
}

print "\nDeleting $topath" . "...\n";
remove_tree($topath);                             # start again on deploy folder
if (-d $topath) {
   print "Directory deletion unsuccessful - quitting...\n";
   exit(1);
}

print "Deleting $rollback_path" . "...\n";
remove_tree($rollback_path);                      # start again on rollback folder
if (-d $rollback_path) {
   print "Directory deletion unsuccessful - quitting...\n";
   exit(1);
}

print "Deleting and recreating $database_root" . "...\n";
remove_tree($database_root);                      # start again on database folder
if (-d $database_root) {
   print "Directory deletion unsuccessful - quitting...\n";
   exit(1);
}
`mkdir "$database_root"`;
if (! -d $database_root) {
   print "Directory creation unsuccessful - quitting...\n";
   exit(1);
}

for( $a = 0; $a <= $size; $a = $a + 1 ){
  $row = $results[$a];
  chomp $row;
  $row =~ s{/}{\\}g;
  
  if ($row =~ /^diff --git a\\([\w|\-|\.|\s|\\]*)\sb\\/) {  # filename row?
    $filename = $1;
	print "\n*** $filename\n";

    @c = fileparse($filename);                      # isolate path from filename into array, build vars
    $from = $frompath . $filename;                  # local build folder filepath
    $todir = $topath . $c[1];                       # deploy folder full folder path
    $to = $topath . $filename;                      # deploy folder full file path
  
	if ($filename =~ /^Database/) {                         # does it begin with Database?
      $in_db_file = "true";                                 # start processing set for this file
	  $file_conflict = "false";
 	} else {
	  $in_db_file = "false";                                # nope - must be ShareView or something...
	}

	if ($filename =~ /^ShareView/) {                        # does it begin with Shareview?
      $in_sv_file = "true";                                 # start processing set for this file
	  $file_conflict = "false";
 	} else {
	  $in_sv_file = "false";                                # nope - must be something else...
	}

    if ($in_db_file eq "true") {
      if (($filename !~ /\.sqlproj$/) && ($filename !~ /\.csproj$/) && ($results[$a + 1] !~ /deleted\sfile\smode\s100644$/)) {  # peek ahead one element in array looking for delete flag
	                                                                                              # also, ignore *.sqlproj file(s)
		$database_files = $database_files + 1;
		($parse_filename, $directories, $suffix) = fileparse($from);
		$out_dir = $database_root . $c[1];
		$out_file = $out_dir . $c[0];
        if (! -e $from) {
		  print " XXXXXXX Not exists - ";
		  print "\n$from  ";
		} else {
		  if (! -d $out_dir) {
		    print "Creating $out_dir\n";
		    `mkdir "$out_dir"`;
		  }
		  print "copy:   $from \n";
		  print "        $out_file  ";
		  `copy "$from" "$out_file"`;
		  $index = $index + 1;
        }
      }
    } elsif ($in_sv_file eq "true") {
      $shareview_files = $shareview_files + 1;
      $rollback_src = $server_dir . "\\" . $filename;                # (raw) network target deploy file full path
      $rollback_src =~ s/ShareView\\web\\sports_1_7\\//;             # screen out extra pathing from deploy file
      $rollback_path_sub = $rollback_path . $c[1];                   # build rollback folder full file path
	  
      if (! -d $todir) {
        print "\nmkdir " . "\"" . $todir . "\"\n";                     # Deploy folder
        `mkdir "$todir"`;
      }
      print "copy \"" . $from . "\"" . " \"" . $to . "\"\n";
      if (-e $from) {                                                # do copy unless file not found in local build tree
        `copy "$from" "$to"`;
      } else {
        print " ***** - ERROR - $from does not exist.  Quitting.\n";                  # Oops!
        exit(1);
      }

      if (! -d $rollback_path_sub) {
        print "\nmkdir " . "\"" . $rollback_path_sub . "\"\n";         # Rollback folder creation and file copy
        `mkdir "$rollback_path_sub"`;
      }
      $dest = $rollback_path_sub . $c[0];
      print "copy \"" . $rollback_src . "\"" . " \"" . $dest . "\"\n";
      if (-e $rollback_src) {
        `copy "$rollback_src" "$dest"`;
      } else {
        print " ***** - WARNING - $rollback_src does not exist.  File is considered new, so not on destination.  Ignoring.\n";
        $rollback_warn = $rollback_warn + 1;
      }
	}
	
	print "\n";
  } elsif ($row =~ /<<<<<<</) {
    if ($file_conflict ne "true") {
      print "   >>>>>> CONFLICT!";
      $file_conflict = "true";
      $conflict_flag = "true";
      $conflict_count++;
    }
  }
}

print "\n*****************************************\n\n";
print "***** Shareview files - $shareview_files\n";
print "***** Database files - $database_files\n";
