$branch = shift;
$Override_FI = shift;
chomp($branch);
chomp($Override_FI);

print "branch - $branch\n";

# exit;

if ($Override_FI ne "Override") {
  if ($branch =~ /^origin\/hotfix\//i) {
    print "Checking to see if remotes/origin/master appears in list of ummerged branches...\n";
    @ret_ary = `git branch -a --no-merged`;
    foreach $loop (@ret_ary) {
      chomp($loop);
      $loop =~ s/^\s*//;
      if ($loop eq "remotes/origin/master") {
        print "#### ERROR : Found remotes/origin/master in list.  Needs forward integration from master.  Quitting...\n";
    	exit(1);
      }
    }
  } elsif ($branch =~ /^origin\/feature\//i) {
    print "Checking to see if remotes/origin/develop appears in list of ummerged branches...\n";
    @ret_ary = `git branch -a --no-merged`;
    foreach $loop (@ret_ary) {
      chomp($loop);
      $loop =~ s/^\s*//;
      if ($loop eq "remotes/origin/develop") {
        print "#### ERROR : Found remotes/origin/develop in list.  Needs forward integration from develop.  Quitting...\n";
    	exit(1);
      }
    }
  } else {
    print "#### WARNING - specified branch ($branch) does not begin with origin/hotfix or origin/feature - bypassing this test...\n";
  }

  print "#### - merge check - OK\n";
} else {
  print "#### - Forward Integration need test bypassed\n";
}
